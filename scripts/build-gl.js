const exec = require('child_process').exec;
const path = require('path');
const fs = require('fs');
const jsdom = require('jsdom');
const argv = require('minimist')(process.argv.slice(2));

const distFolder = path.resolve(__dirname, '../dist/posts-app');
const index = path.resolve(distFolder, 'index.html');

const appHref = argv.appHref || '';
const manifestPath = path.resolve(distFolder, 'manifest.json');

console.log('Building prod...');
exec(`ng build --prod --base-href="/${appHref}"`, (error) => {
  if (error) {
    console.error(error);
    return;
  }
  console.log('Built prod successfully');
  updateManifestLink();
  UpdateManifestContent();
});

function UpdateManifestContent() {
  console.log('Updating manifest content');
  const manifestJson = fs.readFileSync(manifestPath);
  var manifest = JSON.parse(manifestJson);
  manifest.start_url = `/${appHref}/`;

  fs.writeFileSync(manifestPath, JSON.stringify(manifest));
  console.log('Manifest content updated');
}

function updateManifestLink() {
  console.log('Updating manifest link');
  const manifestHref = `${appHref}/manifest.json`;
  const page_content = fs.readFileSync(index, 'utf-8');
  const dom = new jsdom.JSDOM(page_content);
  const link = dom.window.document.querySelector('head link[rel="manifest"]');
  console.log(`Current link: ${link.getAttribute('href')}`);
  link.setAttribute('href', manifestHref);
  const indexHTML = dom.serialize() ;
  console.log(`New href ${manifestHref}`);
  fs.writeFileSync(index, indexHTML, { flag: 'w' });
  console.log('Manifest link updated');
}
