import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as AuthAction from './actions';
import User, { LoginData, SignUpData } from '../user.model';

export interface AuthState {
  user: User;
  loading: boolean;
  loginData?: LoginData;
  signUpData?: SignUpData;
}

export interface AppState {
  authentication: AuthState;
}

const initialState: AuthState = {
  user: null,
  loading: false
};

export function reducer(state = initialState, action: any) {
  switch (action.type) {
    case AuthAction.AUTHENTICATE: return {
      ...state,
      loginData: action.payload,
      loading: true
    };
    case AuthAction.AUTHENTICATED_SUCCESSFULLY:
    case AuthAction.SET_USER_SUCCESSFULLY: return {
      ...state,
      user: action.payload,
      loading: false
    };

    case AuthAction.SIGN_UP: return {
      ...state,
      signUpData: action.payload,
      loading: true
    };

    case AuthAction.LOG_OUT_SUCCESS: return {
      ...state,
      user: null
    };

    case AuthAction.AUTH_CURRENT: return {
      ...state,
      loading: true,
      user: null
    };
  }
  return state;
}

const authSelector = createFeatureSelector<AuthState>('authentication');
export const getUserState = createSelector(authSelector, state => state.user);
