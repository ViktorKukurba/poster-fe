import { Action } from '@ngrx/store';
import User, { LoginData, SignUpData } from '../user.model';

export const AUTHENTICATE = 'authenticate user';
export const AUTHENTICATED_SUCCESSFULLY = 'user authenticated successfully';
export const SIGN_UP = 'register user';
export const LOG_OUT = 'logout user';
export const LOG_OUT_SUCCESS = 'logout user success';
export const AUTH_CURRENT = '[Auth] authenticate current user';
export const SET_USER_SUCCESSFULLY = '[Auth] set user successfully';

export interface AuthState {
  authenticated: boolean;
  user: User;
}

export class Authenticate implements Action {
  readonly type: string = AUTHENTICATE;

  constructor(public payload: LoginData) {}
}

export class AuthenticatedSuccessfully implements Action {
  readonly type: string = AUTHENTICATED_SUCCESSFULLY;

  constructor(public payload: User) {}
}

export class SetUserSuccessfully implements Action {
  readonly type = SET_USER_SUCCESSFULLY;

  constructor(public payload: User) {}
}

export class SignUp implements Action {
  readonly type = SIGN_UP;

  constructor(public payload: SignUpData) {}
}

export class Logout implements Action {
  readonly type: string = LOG_OUT;
}

export class LogoutSuccess implements Action {
  readonly type: string = LOG_OUT_SUCCESS;
}

export class AuthCurrentUser implements Action {
  readonly type = AUTH_CURRENT;
}

export type AuthAction = Authenticate | AuthenticatedSuccessfully | SignUp | Logout | SetUserSuccessfully;
