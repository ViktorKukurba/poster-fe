import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import * as authActions from './actions';
import { AuthService } from '../auth.service';

import { NotificationsService } from 'src/app/services/notifications.service';

type Action = authActions.AuthAction;

@Injectable()
export class AuthEffects {
  constructor(private actions: Actions,
    private auth: AuthService,
    private notifyService: NotificationsService) {}

  @Effect()
  authenticate: Observable<Action> = this.actions.pipe(
    ofType(authActions.AUTHENTICATE),
    map((action: authActions.Authenticate) => action.payload),
    mergeMap((payload) => this.auth.login(payload)),
    map(user => new authActions.AuthenticatedSuccessfully(user)));

  @Effect()
  signup: Observable<Action> = this.actions.pipe(
    ofType(authActions.SIGN_UP),
    map((action: authActions.SignUp) => action.payload),
    mergeMap((payload) => this.auth.signup(payload)),
    map(user => new authActions.SetUserSuccessfully(user)));

  @Effect()
  logout: Observable<Action> = this.actions.pipe(
    ofType(authActions.LOG_OUT),
    switchMap(() => this.auth.logout()),
    map(() => new authActions.LogoutSuccess()));

  @Effect()
  currentUser: Observable<Action> = this.actions.pipe(
    ofType(authActions.AUTH_CURRENT),
    switchMap(() => this.auth.currentUser()),
    filter(user => Boolean(user)),
    map((user) => new authActions.SetUserSuccessfully(user)));

  @Effect()
  authenticated = this.actions.pipe(
    ofType(authActions.AUTHENTICATED_SUCCESSFULLY),
    map((action: authActions.Authenticate) => action.payload),
    map(() => {
      this.notifyService.notify('Authenticated successfully!');
      return {type: '[Notification] Authnticated'};
    }));

  @Effect()
  loggedout = this.actions.pipe(
    ofType(authActions.LOG_OUT_SUCCESS),
    map(() => {
      this.notifyService.notify('Logged out successfully!');
      return {type: '[Notification] You\'re logged out'};
    }));
}
