import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { SignUp } from '../store/actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output()
  success = new EventEmitter();

  registerForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
    repeatPassword: new FormControl()
  });

  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

  onSubmit() {
    this.store.dispatch(new SignUp(this.registerForm.value));
  }

}
