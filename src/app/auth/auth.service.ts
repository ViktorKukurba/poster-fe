import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NotificationsService } from 'src/app/services/notifications.service';
import User, { LoginData, SignUpData } from './user.model';
import { environment } from 'src/environments/environment';

const uri = `${environment.server}/auth`;

interface AuthResponse {
  success: boolean;
  user: User;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user = new Subject<User>();
  constructor(private http: HttpClient, private notifications: NotificationsService) {
    this.currentUser().subscribe({
      next: (user) => this.user.next(user)
    });
  }

  currentUser() {
    return this.http.get<User>(`${uri}/user`, {withCredentials: true});
  }

  login(loginData: LoginData): Observable<User> {
    return this.http.post<User>(`${uri}/login`, loginData, {withCredentials: true});
  }

  loginFb(): Observable<User> {
    return this.http.get<User>(`${uri}/login/facebook`, {withCredentials: true});
  }

  signup(signUpData: SignUpData): Observable<User> {
    return this.http.post<User>(`${uri}/signup`, signUpData, {withCredentials: true});
  }

  logout() {
    return this.http.get(`${uri}/logout`, {withCredentials: true}).pipe(map(() => {
      this.user.next(null);
    }));
  }
}
