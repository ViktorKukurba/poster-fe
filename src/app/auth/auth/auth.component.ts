import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';

import { AuthState, getUserState } from '../store/reducer';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AuthComponent>,
    private store: Store<AuthState>) {}

  ngOnInit() {
    this.store.select(getUserState).pipe(filter(u => Boolean(u))).subscribe(() => {
      this.dialogRef.close();
    });
  }
}
