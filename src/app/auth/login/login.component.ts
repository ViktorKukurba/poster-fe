import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import {Authenticate, AuthState } from '../store/actions';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output()
  success = new EventEmitter();

  authFb = `${environment.server}/auth/login/facebook`;

  loginForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(private store: Store<AuthState>) { }

  ngOnInit() {
  }

  onSubmit() {
    this.store.dispatch(new Authenticate(this.loginForm.value));
  }

  loginAsTestUser(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.store.dispatch(new Authenticate(environment.testUser));
  }
}
