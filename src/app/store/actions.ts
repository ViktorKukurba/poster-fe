import { Action } from '@ngrx/store';
import User from '../auth/user.model';

export const LOAD_USERS = '[App] load users';
export const LOADED_USERS = '[App] loaded users';

export class UsersLoad implements Action {
  readonly type = LOAD_USERS;
}

export class UsersLoaded implements Action {
  readonly type = LOADED_USERS;

  constructor(public payload: User[]) {}
}
