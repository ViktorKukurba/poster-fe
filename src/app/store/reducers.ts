import { ActionReducerMap, createSelector } from '@ngrx/store';
import { LOADED_USERS } from './actions';
import User from '../auth/user.model';

export interface AppState {
  users: User[];
}

const initialState: AppState = {
  users: []
};

export function reducer(state = initialState, action: any) {
  switch (action.type) {
    case LOADED_USERS: return {
      ...state,
      users: action.payload
    };
  }
  return state;
}

export interface State {
  app: AppState;
}

export const reducers: ActionReducerMap<State> = {
  app: reducer
};

export const getUsers = createSelector((state: AppState) => state.users);
