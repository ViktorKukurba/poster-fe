import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { LOAD_USERS, UsersLoaded } from './actions';
import { UsersService } from '../services/users.service';
import * as authActions from 'src/app/auth/store/actions';

@Injectable()
export class AppEffects {

  constructor(private actions: Actions, private usersService: UsersService) {}

  @Effect()
  users = this.actions.pipe(
    ofType(LOAD_USERS),
    switchMap(() => this.usersService.getUsers().pipe(
      map(users => new UsersLoaded(users)),
      catchError(e => of({type: 'Users loading error', e}))))
  );

  @Effect()
  authenticate = this.actions.pipe(
    ofType(authActions.AUTHENTICATE),
    map((action: authActions.Authenticate) => action.payload),
    switchMap((payload) => this.usersService.subscribe().pipe(
      catchError(e => of({type: 'Subscription error', e}))
    )));
}
