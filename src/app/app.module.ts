import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import {
  MatButtonModule,
  MatToolbarModule,
  MatSnackBarModule,
} from '@angular/material';

import { AuthModule } from 'src/app/auth/auth.module';
import { PostsModule } from 'src/app/posts/posts.module';

import { AppEffects, reducers } from './store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const baseHref = environment.baseHref || '';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register(`ngsw-worker.js`, { enabled: environment.production }),
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forFeature([AppEffects]),
    AuthModule,
    PostsModule,

    MatButtonModule,
    MatToolbarModule,
    MatSnackBarModule,

    StoreDevtoolsModule.instrument(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
