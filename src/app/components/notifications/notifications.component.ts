import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NotificationsService } from 'src/app/services/notifications.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  constructor(private snackBar: MatSnackBar, private notifications: NotificationsService) { }

  ngOnInit() {
    this.notifications.notification.subscribe(message => {
      this.snackBar.open(message, null, {
        duration: 2000,
        verticalPosition: 'top'
      });
    });
  }
}
