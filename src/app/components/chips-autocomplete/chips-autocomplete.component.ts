import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, Input, OnInit, EventEmitter, Output} from '@angular/core';
import { FormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

/**
 * @title Chips Autocomplete
 */
@Component({
  selector: 'app-chips-autocomplete',
  templateUrl: 'chips-autocomplete.component.html',
  styleUrls: ['chips-autocomplete.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: ChipsAutocompleteComponent,
    multi: true
  }]
})
export class ChipsAutocompleteComponent implements OnInit, ControlValueAccessor {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  inputControl = new FormControl();
  filteredItems: Observable<string[]>;

  onChange: (any) => void;
  // onTouched: () => void;
  value: string[] = [];

  @Output()
  tagsChange = new EventEmitter<string[]>();

  @Input()
  items: Observable<string[]>;

  @Input()
  config;

  itemsSubject = new BehaviorSubject<string[]>([]);

  @ViewChild('inputRef') inputRef: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor() { }

  ngOnInit() {
    this.filteredItems = combineLatest([this.items, this.inputControl.valueChanges.pipe(startWith(''))]).pipe(map(res => {
      const [items, filter] = res;
      return items.filter(r => r.includes(filter) && !this.value.includes(r));
    }));
  }

  get options() {
    return Object.assign({
      placeholder: 'Add specific item'
    }, this.config);
  }

  writeValue(value) {
    this.value = value || [];
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    // this.onTouched = fn;
  }

  add(event: MatChipInputEvent): void {
    // Add value only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.value.push(value.trim());
        this.onChange(this.value);
      }

      if (input) {
        input.value = '';
      }

      this.inputControl.setValue(null);
    }
  }

  remove(value: string): void {
    const index = this.value.indexOf(value);

    if (index >= 0) {
      this.value.splice(index, 1);
      this.onChange(this.value);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.value.push(event.option.viewValue);
    this.inputRef.nativeElement.value = '';
    this.inputControl.setValue(null);
    this.onChange(this.value);
  }
}
