import { Component, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: FileUploaderComponent,
    multi: true
  }]
})
export class FileUploaderComponent implements ControlValueAccessor {

  dropZoneState: string;

  newImagSrc: string;

  @ViewChild('previewImg')
  previewImg: ElementRef;

  value: File|string;

  get imgSrc() {
    if (this.value === 'deleted') {
      return null;
    } else if (typeof this.value === 'string') {
      return this.value;
    }
    return this.newImagSrc;
  }

  onChange: (any) => void;

  constructor() { }

  onFileSelect(evt) {
    this.setPreview(evt.target.files);
  }

  activateDropZone(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    if (evt.dataTransfer.types.some((t: string) => t === 'Files')) {
      this.dropZoneState = 'active';
    }
  }

  leaveDropZone() {
    this.dropZoneState = '';
  }

  dropFile(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    const files = evt.dataTransfer.files;
    this.setPreview(files);
  }

  writeValue(val) {
    this.value = val;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {}

  delete() {
    this.value = 'deleted';
    this.newImagSrc = null;
    this.onChange(this.value);
  }

  private setPreview(files: File[]) {
    if (files.length) {
      this.value = files[0];
      this.onChange(this.value);

      const reader = new FileReader();
      reader.onload = e => {
        this.newImagSrc = (<any>e.target).result;
        this.leaveDropZone();
      };
      reader.readAsDataURL(this.value);
    }
  }
}
