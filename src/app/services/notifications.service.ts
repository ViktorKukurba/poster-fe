import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  notification = new Subject<string>();
  constructor() { }

  notify(message) {
    this.notification.next(message);
  }
}
