import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SwPush } from '@angular/service-worker';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import User from 'src/app/auth/user.model';
import { environment } from 'src/environments/environment';

const uri = `${environment.server}/users`;

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  readonly VAPID_PUB = 'BFhk1pmcHM8LNkJE-t1C6yTG7v0MO2M0BfNc253ju3mmcsy92Fj0VFS22xMnFCo9v2tJT9R0cOOqR6Fs8iWVqzE';

  constructor(private http: HttpClient, private swPush: SwPush) {}

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(uri, {withCredentials: true}).pipe(
      catchError(error => Observable.throw(error)));
  }

  subscribe(): Observable<any> {
    return from(this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUB
    })).pipe(switchMap((sub: PushSubscription) => {
      return this.http.post(`${uri}/subscribe`, sub, {withCredentials: true}).pipe(
        catchError(error => Observable.throw(error)));
    }));
  }
}
