import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AuthComponent } from '../auth/auth/auth.component';
import { AuthState, getUserState } from '../auth/store/reducer';
import { Logout } from '../auth/store/actions';
import User from '../auth/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user: Observable<User>;

  constructor(public dialog: MatDialog, private store: Store<AuthState>) { }

  ngOnInit() {
    this.user = this.store.select(getUserState);
  }

  openLogin() {
    this.dialog.open(AuthComponent, {
      minWidth: '350px',
      panelClass: 'auth-dialog'
    });
  }

  logout() {
    this.store.dispatch(new Logout());
  }
}
