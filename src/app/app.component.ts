import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import User from 'src/app/auth/user.model';
import { AuthState, AuthCurrentUser } from './auth/store/actions';
import { getUserState } from './auth/store/reducer';
import { PostsState } from './posts/store/reducer';
import { PostsLoad } from './posts/store/actions';
import { AppState, UsersLoad } from './store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'posts-app';
  user: Observable<User>;
  users: Observable<User[]>;
  promptEvent: Event;
  constructor(public store: Store<{authenticate: AuthState, posts: PostsState, users: AppState}>) {
    window.addEventListener('beforeinstallprompt', event => {
      this.promptEvent = event;
    });

    // .then(async sub => {
    //   try {
    //     await this.usersService.subscribe(sub).subscribe(res => {
    //       console.log('res', res);
    //     });
    //   } catch (e) {
    //     throw e;
    //   }
    // }).catch(e => {
    //   console.log('error', e);
    // });
  }

  ngOnInit() {
    this.store.dispatch(new AuthCurrentUser());
    this.store.dispatch(new UsersLoad());

    this.user = this.store.select(getUserState);
    this.users = this.store.select<AppState>('app').pipe(map(app => app.users));
    this.user.subscribe(() => {
      this.store.dispatch(new PostsLoad());
    });
  }
}
