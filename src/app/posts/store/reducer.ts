import * as PostActions from './actions';
import Post from '../post.model';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface PostsState {
  posts: Array<Post>;
  loading: boolean;
  submited: boolean;
  filtered: string;
}

const initialState: PostsState = {
  posts: [],
  loading: false,
  submited: false,
  filtered: 'all'
};

export function reducer(state = initialState, action) {
  console.log('action.type', action.type);
  switch (action.type) {
    case PostActions.POSTS_LOAD: return {
      ...state,
      loading: true
    };

    case PostActions.POSTS_LOADED: return {
      ...state,
      posts: action.payload,
      loading: false
    };

    case PostActions.POST_CREATE: return {
      ...state,
      submited: true
    };

    case PostActions.POST_CREATED: return {
      ...state,
      submited: false,
    };

    case PostActions.POST_NEW_RECEIVED: return {
      ...state,
      posts: [action.payload, ...state.posts]
    };

    case PostActions.POST_UPDATED: {
      const post = action.payload;
      const postIndex = state.posts.findIndex(p => {
        return p._id === post._id;
      });
      const posts = [...state.posts];
      posts.splice(postIndex, 1, post);
      return {
        ...state,
        posts,
        loading: false
      };
    }

    case PostActions.POST_DELETED: {
      const postIndex = state.posts.findIndex(p => p._id === action.payload);
      const posts = [...state.posts];
      posts.splice(postIndex, 1);
      return {
        ...state,
        posts,
        loading: false
      };
    }

    case PostActions.POST_FILTERED: return {
      ...state,
      filtered: action.payload
    };

    case PostActions.POSTS_LOAD_FAILED: return {
      ...state,
      loading: false
    };
  }
  return state;
}

const postsFeature = createFeatureSelector<PostsState>('posts');
export const selectPosts = createSelector(postsFeature, state => state.posts.sort((a, b) => {
  return ( + new Date(b.date)) - ( + new Date(a.date));
}));

export const publicPosts = createSelector(selectPosts, state => state.filter(p => p.isPublic));
export const sentPosts = (author: string) => createSelector(selectPosts, state => state.filter(p => p.author === author));
export const receivedPosts = (receiver: string) => createSelector(selectPosts,
  state => state.filter(p => p.receivers && p.receivers.includes(receiver)));

export const submitedPost = createSelector(postsFeature, state => state.submited);
export const postFilter = createSelector(postsFeature, state => state.filtered);

export const filteredPosts = (user) => createSelector(postsFeature, state => state.posts.filter(p => {
  switch (state.filtered) {
    case 'public': return p.isPublic;
    case 'sent': return p.author === user.email;
    case 'received': return p.receivers && p.receivers.includes(user.email);
    default: return state.posts;
  }
}));

export const isLoading = createSelector(postsFeature, state => state.loading);
