import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

import * as PostsActions from './actions';
import { PostsService } from '../posts.service';

import { NotificationsService } from 'src/app/services/notifications.service';

@Injectable()
export class PostsEffects {

  constructor(private actions: Actions,
    private postsService: PostsService,
    private notifyService: NotificationsService) {}

  @Effect()
  posts: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POSTS_LOAD),
      switchMap(() => this.postsService.getPosts().pipe(
        map(posts => new PostsActions.PostsSuccess(posts)),
        catchError(error => of(new PostsActions.PostsFailed(error)))))
  );

  @Effect()
  postsFailed: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POSTS_LOAD_FAILED),
    map(({payload}: any) => {
      this.notifyService.notify(`Loading failed. ${payload.error}`);
      return {type: '[Notification] Posts loading failed'};
    })
  );

  @Effect()
  createPost: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POST_CREATE),
    switchMap(({payload}: PostsActions.PostCreate) => this.postsService.createPost(payload).pipe(
    map(post => new PostsActions.PostCreatedSuccessfully(post)),
    catchError(error => of(new PostsActions.PostCreatedFailed(error))
  ))));

  @Effect()
  createdPost: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POST_CREATED),
    map(() => {
      this.notifyService.notify('Post created!');
      return {type: '[Notification] Post created'};
    }));

  @Effect()
  createdPostFailed: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POST_FAILED),
    map(({payload}: any) => {
      this.notifyService.notify(`Post failed! ${payload.error}`);
      return {type: '[Notification] Post failed'};
    }));

  @Effect()
  updatePost: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POST_UPDATE),
    switchMap(({payload}: PostsActions.PostUpdate) => this.postsService.updatePost(payload.id, payload.formData).pipe(
    map(post => new PostsActions.PostUpdatedSuccessfully(post)),
    catchError(error => of(new PostsActions.PostCreatedFailed(error)))))
  );

  @Effect()
  deletePost: Observable<Action> = this.actions.pipe(
    ofType(PostsActions.POST_DELETE),
    switchMap(({payload}: PostsActions.PostDelete) => this.postsService.deletePost(payload)),
    map(postId => new PostsActions.PostDeletedSuccessfully(postId))
  );
}
