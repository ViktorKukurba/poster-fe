import { Action } from '@ngrx/store';
import Post from '../post.model';

export const POSTS_LOAD = '[Posts] posts load';
export const POSTS_LOADED = '[Posts] posts loaded';
export const POST_CREATE = '[Posts] post create';
export const POST_CREATED = '[Posts] post created';
export const POST_NEW_RECEIVED = '[Posts] new post received';
export const POST_FAILED = '[Posts] post failed';
export const POST_UPDATE = '[Posts] post update';
export const POST_UPDATED = '[Posts] post updated';
export const POST_DELETE = '[Posts] post delete';
export const POST_DELETED = '[Posts] post deleted';
export const POSTS_LOAD_FAILED = '[Posts] posts load failed';
export const POST_FILTERED = '[Posts] posts filtered';

export class PostsLoad implements Action {
  readonly type = POSTS_LOAD;
}

export class PostsSuccess implements Action {
  readonly type = POSTS_LOADED;

  constructor(public payload: Post[]) {}
}

export class PostsFailed implements Action {
  readonly type = POSTS_LOAD_FAILED;

  constructor(public payload: Post[]) {}
}

export class PostCreate implements Action {
  readonly type = POST_CREATE;

  constructor(public payload: FormData) {}
}

export class PostCreatedSuccessfully implements Action {
  readonly type = POST_CREATED;

  constructor(public payload: Post) {}
}

export class NewPostReceived implements Action {
  readonly type = POST_NEW_RECEIVED;

  constructor(public payload: Post) {}
}

export class PostUpdate implements Action {
  readonly type = POST_UPDATE;

  constructor(public payload: {id: string, formData: FormData}) {}
}

export class PostUpdatedSuccessfully implements Action {
  readonly type = POST_UPDATED;

  constructor(public payload: Post) {}
}

export class PostCreatedFailed implements Action {
  readonly type = POST_FAILED;

  constructor(public payload: any) {}
}

export class PostDelete implements Action {
  readonly type = POST_DELETE;

  constructor(public payload: string) {}
}

export class PostDeletedSuccessfully implements Action {
  readonly type = POST_DELETED;

  constructor(public payload: string) {}
}

export class PostsFiltered implements Action {
  readonly type = POST_FILTERED;

  constructor(public payload: string) {}
}

export type PostsActions = PostsLoad | PostsSuccess | PostCreate | PostCreatedSuccessfully | PostsFiltered;
