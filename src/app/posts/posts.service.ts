import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import Post from './post.model';
import { environment } from 'src/environments/environment';

const uri = `${environment.server}/posts`;

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private postsSubject = new BehaviorSubject<Post[]>([]);
  public posts = this.postsSubject.asObservable();
  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${uri}`, {withCredentials: true}).pipe(catchError(({error}) => throwError(error)));
  }

  updatePost(postId, updatedPost: FormData): Observable<Post> {
    return this.http.put<Post>(`${uri}/post/${postId}`, updatedPost, {withCredentials: true}).pipe(
      catchError(({error}) => throwError(error)));
  }

  createPost(form: FormData): Observable<Post> {
    return this.http.post<Post>(`${uri}/post`, form, {withCredentials: true}).pipe(
      catchError(({error}) => throwError(error)));
  }

  deletePost(postId: string): Observable<string> {
    return this.http.delete<Post>(`${uri}/post/${postId}`, {withCredentials: true}).pipe(map(post => {
      if (post) {
        return postId;
      } else {
        throwError({error: 'Post was not deleted'});
      }
    }),
    catchError(({error}) => throwError(error)));
  }
}
