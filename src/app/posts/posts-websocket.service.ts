import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import Post from './post.model';
import { Subject } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class PostsWebSocketService {

  private webSocket: SocketIOClient.Socket;
  postsCreated = new Subject<Post>();
  constructor() {
    this.webSocket = io.connect(environment.ws);

    this.webSocket.on('post.created', (post: Post) => {
      this.postsCreated.next(post);
    });
  }
}
