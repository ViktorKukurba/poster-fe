import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import User from 'src/app/auth/user.model';

@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.scss']
})
export class PostDialogComponent implements OnInit {

  user: Observable<User>;
  receivers: Observable<User[]>;

  constructor(@Inject(MAT_DIALOG_DATA) data) {
      this.user = data.user;
      this.receivers = data.receivers;
    }

  ngOnInit() {
  }
}
