import { TestBed } from '@angular/core/testing';

import { PostsWebSocketService } from './posts-websocket.service';

describe('PostsWebSocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostsWebSocketService = TestBed.get(PostsWebSocketService);
    expect(service).toBeTruthy();
  });
});
