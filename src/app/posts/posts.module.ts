import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatExpansionModule,
  MatTabsModule,
  MatAutocompleteModule,
  MatChipsModule,
  MatIconModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { ChipsAutocompleteComponent } from 'src/app/components/chips-autocomplete/chips-autocomplete.component';
import { FileUploaderComponent } from 'src/app/components/file-uploader/file-uploader.component';

import { PostListComponent } from './post-list/post-list.component';
import { PostViewComponent } from './post-view/post-view.component';
import { PostFormComponent } from './post-form/post-form.component';
import { PostsPageComponent } from './posts-page/posts-page.component';
import { PostDialogComponent } from './post-dialog/post-dialog.component';

import { reducer } from './store/reducer';
import { PostsEffects } from './store/effects';

@NgModule({
  declarations: [
    ChipsAutocompleteComponent,
    PostListComponent,
    PostViewComponent,
    PostFormComponent,
    PostsPageComponent,
    PostDialogComponent,
    FileUploaderComponent
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forFeature('posts', reducer),
    EffectsModule.forRoot([PostsEffects]),
    FormsModule,
    CommonModule,
    FlexLayoutModule,

    MatInputModule,
    MatCardModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule
  ],
  exports: [
    PostsPageComponent
  ],
  entryComponents: [PostDialogComponent]
})
export class PostsModule { }
