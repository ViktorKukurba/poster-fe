import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import Post from '../post.model';
import User from 'src/app/auth/user.model';
import { PostsState, submitedPost } from '../store/reducer';
import { PostUpdate, PostCreate} from '../store/actions';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

  postForm = new FormGroup({
    title: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    receivers: new FormControl([]),
    img_src: new FormControl(),
    isPublic: new FormControl(true, Validators.required)
  });

  @Input()
  post: Post;

  @Input()
  user: User;

  @Input()
  receivers: Observable<User[]>;

  disabled: Observable<boolean>;

  submitting = false;

  allReceivers: Observable<string[]>;

  postModel = new Post();

  autoConfig = {
    placeholder: 'Add specific reciever...'
  };

  constructor(private store: Store<PostsState>) { }

  ngOnInit() {
    this.disabled = this.store.select(submitedPost).pipe(filter(submitted => submitted));

    this.disabled.subscribe((s) => {
      this.submitting = s;
      if (!s) {
        this.postForm.reset();
      }
    });

    if (this.post) {
      this.postForm.patchValue(this.post);
    }

    this.allReceivers = this.receivers.pipe(map(users => users.map(u => u.email)));
  }

  get isSubmitDisabled() {
    return !this.postForm.valid || !this.postForm.dirty;
  }

  get controls() { return this.postForm.controls; }

  onPostSubmit() {
    if (this.postForm.invalid) {
      return;
    }

    const fd = this.getFormData();
    if (this.post) {
      this.store.dispatch(new PostUpdate({id: this.post._id, formData: fd}));
    } else {
      this.store.dispatch(new PostCreate(fd));
    }
  }

  private getFormData(): FormData {
    const fd = new FormData();
    Object.entries(this.controls).forEach(([key, control]) => {
      const value = control.value;
      fd.append(key, value instanceof Array ? JSON.stringify(value) : value);
    });
    return fd;
  }
}
