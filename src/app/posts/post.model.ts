export default class Post {
  title: string;
  author: string;
  content: string;
  _id?: string;
  isPublic = true;
  image?: File;
  img_src?: string;
  receivers: string[];
  date?: string;
}
