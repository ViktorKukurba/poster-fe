import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatDialog, MatDialogRef, MatButtonToggleChange } from '@angular/material';
import { Observable } from 'rxjs';
import { filter, distinctUntilChanged } from 'rxjs/operators';

import User from 'src/app/auth/user.model';
import { PostDialogComponent } from '../post-dialog/post-dialog.component';
import { PostsState, submitedPost } from '../store/reducer';
import { PostsFiltered } from '../store/actions';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss']
})
export class PostsPageComponent implements OnInit {

  @Input()
  user: Observable<User>;

  @Input()
  users: Observable<User[]>;

  filterOptions = ['all', 'sent', 'received', 'public'];

  dialogRef: MatDialogRef<PostDialogComponent>;

  constructor(public dialog: MatDialog, private store: Store<PostsState>) { }

  ngOnInit() {
    this.store.select(submitedPost).pipe(distinctUntilChanged(), filter(submitted => !submitted)).subscribe((s) => {
      console.log('sss', s);
      if (this.dialogRef) {
        this.dialogRef.close();
      }
    });
  }

  openPostCreate() {
    this.dialogRef = this.dialog.open( PostDialogComponent, {
      width: '50%',
      minWidth: '350px',
      maxWidth: '1000px',
      panelClass: 'post-dialog',
      data: {
        receivers: this.users,
        user: this.user
      }
    });
  }

  setFilter(evt: MatButtonToggleChange) {
    this.store.dispatch(new PostsFiltered(evt.value));
  }
}
