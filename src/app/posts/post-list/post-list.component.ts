import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import Post from '../post.model';
import { PostsState, publicPosts, filteredPosts, isLoading } from '../store/reducer';
import User from 'src/app/auth/user.model';
import { switchMap } from 'rxjs/operators';
import { PostsWebSocketService } from '../posts-websocket.service';
import { NewPostReceived } from '../store/actions';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  @Input()
  user: Observable<User>;

  @Input()
  receivers: Observable<User[]>;

  posts: Observable<Post[]>;

  loading: Observable<boolean>;

  constructor(private store: Store<PostsState>, private postsWSS: PostsWebSocketService) { }

  ngOnInit() {
    this.posts = this.user.pipe(switchMap(user => {
      if (user) {
        return this.store.select(filteredPosts(user));
      }
      return this.store.select(publicPosts);
    }));

    this.postsWSS.postsCreated.subscribe(p => {
      this.store.dispatch(new NewPostReceived(p));
    });

    this.loading = this.store.select(isLoading);
  }
}
