import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import Post from '../post.model';
import { AuthState } from 'src/app/auth/store/reducer';
import { PostDelete } from '../store/actions';
import User from 'src/app/auth/user.model';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  @Input()
  post: Post;

  @Input()
  user: User;

  @Input()
  receivers: Observable<User[]>;

  editable: false;

  constructor(private store: Store<AuthState>) { }

  ngOnInit() {
  }

  getFromNow(date) {
    return moment(date).fromNow();
  }

  deletePost() {
    this.store.dispatch(new PostDelete(this.post._id));
  }
}
